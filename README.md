# Marking templates
The automatic form generation script and the templates themselves are tightly coupled. The
former expects the latter to contain specific content and format at the specific location.

Under no circumstances are the templates to be modified by staff. Do not add or remove
rows, columns, cells, worksheets or anything else, as this risks breaking the form
generation functionality, or the marks extraction functionality, or both! In summary, do
not make any changes of any nature no matter how trivial they may seem.

## Manual form creation
Sometimes we have last-minute marker allocation changes, for example because a member
of staff is unavailable to undertake assessment and someone needs to be allocated to
replace them. In this scenario it is likely that form generation has not created the
correct form for the substitute assessor. If this happens, it is perfectly OK to provide
the template itself to the substitute assessor, asking them to fill-in student details
manually. However, the template will be unlocked and it is important to warn the assessor
that, other than filling-in student details, they should not make any other modifications
to the template for the same reasons as above.

# Uploading Marks to Blackboard
Marks can be uploaded to Blackboard using a CSV file.

This CSV file can be either comma- or tab-delimited.

The example file in this directory is tab-delimited.

## Example CSV file
The CSV file needs to contain exactly 2 "columns".

The first row is a header row and looks like this:

```
"Username"	"IPP Technical Mark [Total Pts: 100 Score] |655541"
```

The first column contains the student's username enclosed in double quotes, without
the @bristol.ac.uk suffix. This can be for example `"ab12345"`.

The second column contains the actual mark, for example `"56"`.

The correct title of the second column can be obtained by exporting the Grade Centre from
Blackboard through the Work Offline function. This will lead to a larger and more complex
CSV file. We only need this file in order to obtain the correct title for the column
carrying the actual mark. The rest of this file can be safely ignored.

In each row, fields must be separated by _exactly one_ tab character. Multiple tabs will
lead to an import error.

## Partial uploading of marks
It is _not_ required to import all marks into the grade centre in a single go.

The following logic applies:

* It is possible to upload marks for _some_ students.
* If the upload file contains a mark for a student where a mark already exists, the latter
  will be over-written by the former. So, it is possible to update marks using this
  process.
